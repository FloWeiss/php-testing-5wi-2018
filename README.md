# Automated tests using PHP

This is a sample project to learn about PHPUnit testing and
continuous integration.

## Installation

* [install composer][1]
* Fork this project on gitlab.com
* git clone your repository
* run `composer install` inside the project directory

[1]: https://getcomposer.org/doc/00-intro.md#installation-windows

## Usage

### Run calculator

`php bin/console calc:add num1 num2`

### Run tests

`php bin/phpunit`

### Run php-cs-fixer check

`php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix --verbose --dry-run`
